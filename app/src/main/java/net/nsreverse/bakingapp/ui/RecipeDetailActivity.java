package net.nsreverse.bakingapp.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import net.nsreverse.bakingapp.data.utils.ApplicationInstance;
import net.nsreverse.bakingapp.R;
import net.nsreverse.bakingapp.data.utils.JsonParser;
import net.nsreverse.bakingapp.data.utils.RuntimeCache;
import net.nsreverse.bakingapp.model.InstructionStep;
import net.nsreverse.bakingapp.model.Recipe;
import net.nsreverse.bakingapp.ui.adapters.RecipeDetailAdapter;
import net.nsreverse.bakingapp.ui.fragments.RecipeDetailFragment;
import net.nsreverse.bakingapp.ui.fragments.RecipeStepDetailFragment;

/**
 * This class represents the recipe detail activity. On phones it only displays the steps in the
 * RecyclerView. On tablets, it additionally displays the step detail fragment.
 *
 * @author Robert
 * Created on 6/7/2017
 */
public class RecipeDetailActivity extends AppCompatActivity
                                  implements RecipeDetailAdapter.Delegate {

    public static final String POSITION_KEY = "POSITION";

    private static boolean sHasInitialized = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);

        if (getSupportActionBar() != null && getIntent().hasExtra(POSITION_KEY)) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            int position = getIntent().getIntExtra(POSITION_KEY, 0);

            Recipe selectedRecipe = RuntimeCache.recipes[position];

            getSupportActionBar().setTitle(selectedRecipe.getName());

            RecipeDetailFragment fragment = new RecipeDetailFragment();
            fragment.setSelectedIndex(position);

            if (!sHasInitialized) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.detail_fragment_container, fragment)
                        .commit();

                sHasInitialized = true;
            }
            else {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.detail_fragment_container, fragment)
                        .commit();
            }
        }
    }

    @Override
    public void itemSelected(InstructionStep step, int rowPosition) {
        if (!ApplicationInstance.isTablet) {
            Intent intent = new Intent(this, RecipeStepDetailActivity.class);
            intent.putExtra(RecipeStepDetailActivity.RECIPE_INDEX_KEY,
                    getIntent().getIntExtra(POSITION_KEY, 0));
            intent.putExtra(RecipeStepDetailActivity.INSTRUCTION_INDEX_KEY, rowPosition);
            startActivity(intent);
        }
        else {
            int position = getIntent().getIntExtra(POSITION_KEY, 0);

            Recipe selectedRecipe = RuntimeCache.recipes[position];
            InstructionStep currentStep = selectedRecipe.getSteps()[rowPosition];

            RecipeStepDetailFragment fragment = new RecipeStepDetailFragment();
            fragment.setDataSource(currentStep);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.step_detail_fragment_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
