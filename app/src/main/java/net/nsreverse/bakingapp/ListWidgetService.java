package net.nsreverse.bakingapp;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import net.nsreverse.bakingapp.data.utils.JsonParser;
import net.nsreverse.bakingapp.data.utils.RuntimeCache;
import net.nsreverse.bakingapp.model.Recipe;
import net.nsreverse.bakingapp.ui.RecipeDetailActivity;

/**
 * This service populates initial data in a widget.
 *
 * @author Robert
 * Created on 6/9/2017.
 */
public class ListWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new ListRemoteViewsFactory(this.getApplicationContext());
    }
}

class ListRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    Context context;
    Recipe[] recipes;

    public ListRemoteViewsFactory(Context context) {
        this.context = context;
    }

    @Override
    public void onCreate() { }

    @Override
    public void onDataSetChanged() {
        SharedPreferences preferences = context.getSharedPreferences("BakingAppPreferences",
                Context.MODE_PRIVATE);
        String storedJson = preferences.getString("JSON", "Default value");

        RuntimeCache.recipes = JsonParser.recipesWithJsonNoImage(context, storedJson);
        recipes = JsonParser.recipesWithJsonNoImage(context, storedJson);
    }

    @Override
    public void onDestroy() { recipes = null; }

    @Override
    public int getCount() {
        if (recipes != null) {
            return recipes.length;
        }

        return 0;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        if (recipes == null) return null;

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.item_widget_recipe);
        views.setTextViewText(R.id.text_view_widget_title, recipes[position].getName());

        return views;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
