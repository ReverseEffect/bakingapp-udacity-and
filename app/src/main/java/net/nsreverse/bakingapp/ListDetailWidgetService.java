package net.nsreverse.bakingapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import net.nsreverse.bakingapp.data.utils.JsonParser;
import net.nsreverse.bakingapp.data.utils.RuntimeCache;
import net.nsreverse.bakingapp.model.Ingredient;
import net.nsreverse.bakingapp.model.Recipe;

/**
 * This service populates detail data in a widget.
 *
 * @author Robert
 * Created on 6/9/2017.
 */
public class ListDetailWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new ListDetailRemoteViewsFactory(getApplicationContext(), intent);
    }
}

class ListDetailRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    Context context;
    Recipe[] recipes;
    Ingredient[] ingredients;

    int index;

    public ListDetailRemoteViewsFactory(Context context, Intent intent) {
        this.context = context;

        index = intent.getIntExtra("INDEX", 0);
    }

    @Override
    public void onCreate() { }

    @Override
    public void onDataSetChanged() {
        SharedPreferences preferences = context.getSharedPreferences("BakingAppPreferences",
                Context.MODE_PRIVATE);
        String storedJson = preferences.getString("JSON", "Default value");

        RuntimeCache.recipes = JsonParser.recipesWithJsonNoImage(context, storedJson);
        recipes = JsonParser.recipesWithJsonNoImage(context, storedJson);
        ingredients = recipes[index].getIngredients();
    }

    @Override
    public void onDestroy() {
        recipes = null;
        ingredients = null;
    }

    @Override
    public int getCount() {
        if (ingredients != null) {
            return ingredients.length;
        }

        return 0;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        if (ingredients == null) return null;

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.item_widget_recipe);
        views.setTextViewText(R.id.text_view_widget_title_detail_main,
                ingredients[position].getName());

        return views;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
